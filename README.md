# GitLab CI/CD

- GitLab CI/CD is also another CI/CD plateform like Jenkins
- CI/CD is in the heart of DevOps process.
- It is a method to frequently deliver application changes to customer end in automated way.
- Complete CI/CD Process

  - CI - Continuos Integration

    - Code Push to Remote Repo
    - Test
    - Build & Package
    - Artifact Repository

  - CD - Continuos Deployment

    - Deploy to Dev/Staging
    - Testing UI and Changes
    - Deploy to Prod

  - CD - Continuos Delivery
    - Deploy to Dev/Staging
    - Testing UI and Changes
    - Manual agreement by someone responsible
    - Deploy to Prod

# GitLab CICD vs Jenkins vs Azure DevOps

    - GitLab CICD
        1. GitLab is new to industry and its community is in growing phase.
        2. Many Feature Built-In: Self-Monitoring, Container Registry, Docker CI Runner etc.
        3. Allows Keeping CICD and Code Management at same place
        4. Open Source or Commercial (Both) and Self-Hosted or SAAS

    - Jenkins
        1. Jenkins is old, industry leader, powerful, open-source tool and have big community.
        2. Feature can be integrated through multiple plugins; Maintenance Required.
        3. Only CICD Tool, Code mangement not possible
        4. Self Hosting Required and is only option (OpenSource/Self-Managed)

    - Azure DevOps
        1. Azure DevOps is cloud based CICD tools and community still growing
        2. Best Integration for features can be easily possible with Microsoft Services. Other tool integrtion not so easy to integrate.
        3. Commercial/SAAS Tool

# Job Name Limitations
- We Can't Use these keywords as jobs-name
    - image, services, stages, types, before_script, after_script, variables
    - cache, include, true, false, nil

# Basic GitLab Terminology and Workflow
- .gitlab-ci.yml -> It is a yaml based scripted pipeline file where we define all the action or workflows. (Jenkinsfile in Jenkins)
- Jobs -> Jobs are the most fundamental building block of pipelines. (Steps in terms of Jenkinsfile)
    - example 
        ```yml
            job1:
                script: "execute-script-for-job-1"
            job2:
                script: "execute-script-for-job-2"
        ```
![GitLab-Jobs](images/job.png)

# Branch Refrence
- Master - This Branch contain CICD knowledge - <a href="https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD/-/blob/master/" target="_blank">(Click Here)</a>  
- Basic-Pipeline - This Branch is all about GitLab Script Configuration file ".gitlab-ci.yml" - <a href="https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD/-/tree/basic-pipeline" target="_blank">(Click Here)</a>
- GitLab-Architecture - This Branch is all about GitLab Architecture, Runners, Executors and more. - <a href="https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD/-/tree/gitlab-architecture/" target="_blank">(Click Here)</a>
- App-Pipeline-1 - This Branch is used to create CICD for demo nodejs application. - <a href="https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD/-/tree/app-pipeline-1" target="_blank">(Click Here)</a>
- App-Pipeline-2 - This Branch is used to learn advanced and optimization concept for nodejs application CICD. - <a href="https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD/-/tree/app-pipeline-2" target="_blank">(Click Here)</a>

# Repository Refrence For Microservice Application CICD Deployement

## For MonoRepo Microservice Applicaiton Deployement -
This Repository has the specific learning of Microservices Applicaiton Deployment where all the Microservice Code is saved in single git repository - <a href="https://gitlab.com/saini.nirdesh.kumar/microservice-project" target="_blank">(Click Here)</a>

## For PolyRepo Microservice Applicaiton Deployement -
This Repository has the specific learning of Microservices Applicaiton Deployment where all the Microservice Code is saved in different git repositories under in a group - <a href="https://gitlab.com/microservice-cicd3842205" target="_blank">(Click Here)</a>

